"""
collide.py

Collision step of the LBM solver
"""
import tensorflow as tf

from .utils import simple_conv


def get_collide_step(sol, boundary, lattice, Cs, Sc=0.17):
    """Assemble the collision step of the LBM solver"""
    # boundary bounce piece
    f_boundary = tf.multiply(sol.f, boundary)
    f_boundary = simple_conv(f_boundary, lattice.bounce)

    # make vel bforce and rho
    f = sol.f
    vel = sol.velocity
    #rho = self.Rho[0] + 1e-12 # to stop dividing by zero
    rho = sol.rho# to stop dividing by zero

    # collision calc
    NonEq = f - sol.f_eq
    Q = tf.expand_dims(tf.reduce_sum(NonEq*NonEq*sol.EEk, axis=lattice.dim+1), axis=lattice.dim+1)
    Q = tf.sqrt(2.0*Q)
    tau = 0.5*(sol.tau+tf.sqrt(sol.tau*sol.tau + 6.0*Q*Sc/rho))
    f = f - NonEq/tau

    # combine boundary and no boundary values
    f_no_boundary = tf.multiply(f, (1.0 - boundary))
    f = f_no_boundary + f_boundary

    # make step
    collid_step = sol.f.assign(f)
    return collid_step
