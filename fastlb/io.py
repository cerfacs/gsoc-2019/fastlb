"""
io.py

Solve an LBM setup.
This is a high level function which assembles many sublayers.
"""

import os
import numpy as np
from h5py import File

xmf = """<?xml version="1.0" ?>
<!DOCTYPE Xdmf SYSTEM "Xdmf.dtd" []>
<Xdmf xmlns:xi="http://www.w3.org/2003/XInclude" Version="2.2">
  <Domain>
    <Grid Name="" GridType="Uniform">
      <Topology TopologyType="2DCORECTMesh"  Dimensions="{0} {1}"/>
      <Geometry GeometryType="ORIGIN_DXDY">
        <DataItem Name="Origin" Dimensions="2" NumberType="Float" Precision="4" Format="XML">
          0 0
        </DataItem>
        <DataItem Name="Spacing" Dimensions="2" NumberType="Float" Precision="4" Format="XML">
            {2} {2}
        </DataItem>
      </Geometry>
      <Attribute Name="Velocity" AttributeType="Scalar" Center="Node">
        <DataItem Format="HDF" NumberType="Float" Precision="4" Dimensions="{0} {1}">
          {3}:/Velocity
        </DataItem>
      </Attribute>
    </Grid>
  </Domain>
</Xdmf>"""

def dump_h5(dest):
    def dump(sol, sess, it):
        basename = "{0}/solution_{1}".format(dest,str(it).zfill(8))
        frame = sess.run(sol.velocity)
        frame = np.sqrt(np.square(frame[0,:,:,0]) + np.square(frame[0,:,:,1]) + np.square(frame[0,:,:,2]))

        if not os.path.isdir(dest):
            os.mkdir(dest)
        with open('{0}.xmf'.format(basename),'w') as xmf_file:
            xmf_file.writelines(xmf.format(sol.shape[0],sol.shape[1],sol.dx,
                                '{0}.h5'.format(basename.split('/')[-1])))

        with File('{0}.h5'.format(basename),'w') as h5_file:
            h5_file.create_dataset('Velocity', data = frame)
    return dump
