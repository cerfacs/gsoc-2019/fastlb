"""Base class for latflow"""
import numpy as np
import tensorflow as tf
import cv2
import tqdm

from .lattice import D2Q9, D3Q19
from .utils import simple_conv


LATTICES = {
        "D2Q9": D2Q9,
        "D3Q19": D3Q19,
        }

class Solution():
    def __init__(self,
                 lattice,
                 nu,
                 shape,
                 boundary,
                 dx=1.0,
                 dt=1.0,
                 perio=None):

        self.lattice = LATTICES[lattice]()
        self.nu = nu
        self.shape = shape
        self.boundary = tf.constant(boundary)
        self.dt = dt
        self.dx = dx
        self.perio = perio if perio else {'x': False, 'y': False, 'z': False}
        self._inits()

    def _inits(self):
        self.Sc = 0.17
        self.tau = 3.0 * self.nu * self.dt / (self.dx * self.dx) + 0.5
        self.Cs = self.dx/self.dt

        nneigh = self.lattice.nneighbors
        self.f = tf.Variable(np.zeros([1] + self.shape + [nneigh], dtype=np.float32))
        self.velocity = tf.Variable(np.zeros([1] + self.shape + [3], dtype=np.float32))
        self.rho = tf.Variable(np.zeros([1] + self.shape + [1], dtype=np.float32))

        self.boundary_mask = 1.0 - self.boundary

        # TODO This could surely be nicer...
        self.EEk = tf.zeros(self.lattice.dim*[1] + [nneigh])
        for n in range(3):
            for m in range(3):
                if self.lattice.dim == 2:
                    self.EEk = self.EEk + tf.abs(self.lattice.lveloc[:,:,:,n] * self.lattice.lveloc[:,:,:,m])
                elif self.lattice.dim == 3:
                    self.EEk = self.EEk + tf.abs(self.lattice.lveloc[:,:,:,:,n] * self.lattice.lveloc[:,:,:,:,m])

    def initialize_step(self):
        np_f_zeros = np.zeros([1] + self.shape + [self.lattice.nneighbors], dtype=np.float32)
        f_zero = tf.constant(np_f_zeros)
        f_zero = f_zero + self.lattice.weights
        assign_step = self.f.assign(f_zero)
        return assign_step

    @property
    def f_eq(self):
        vel = self.velocity
        vel_dot_vel = tf.expand_dims(
                tf.reduce_sum(vel * vel, axis=self.lattice.dim+1),
                axis=self.lattice.dim+1)
        transp = [0,1,3,2] if self.lattice.dim == 2 else [0,1,2,4,3]
        vel_dot_c = simple_conv(vel, tf.transpose(self.lattice.lveloc, transp))
        return self.lattice.weights * self.rho * (
                1.0 + 3.0 * vel_dot_c / self.Cs +
                4.5 * vel_dot_c * vel_dot_c / (self.Cs * self.Cs) -
                1.5 * vel_dot_vel / (self.Cs * self.Cs))


    @property
    def f_pad(self):
      pad = self.f
      if self.perio['x']:
          pad = tf.concat(axis=1, values=[pad[:,-1:],   pad, pad[:,0:1]])
      else:
          pad = tf.concat(axis=1, values=[pad[:,0:1],   pad, pad[:,-1:]])
      if self.perio['y']:
          pad = tf.concat(axis=2, values=[pad[:,:,-1:], pad, pad[:,:,0:1]])
      else:
          pad = tf.concat(axis=2, values=[pad[:,:,0:1], pad, pad[:,:,-1:]])
      if len(self.f.get_shape()) == 5:
          if self.perio['z']:
              pad = tf.concat(axis=3, values=[pad[:,:,:,-1:], pad, pad[:,:,:,0:1]])
          else:
              pad = tf.concat(axis=3, values=[pad[:,:,:,0:1], pad, pad[:,:,:,-1:]])
      return pad


def mpipartition(lattice, nu, shape, boundary, dx=1.0, dt=1.0):
    """Create solutions partitioned in the stupidest way"""
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    nranks = MPI.COMM_WORLD.Get_size()
    # shape[0] = k*nranks + p
    shape = np.array(shape)
    k = shape[0] // nranks
    if shape[0] % nranks:
        k += 1
        shapes = [[k, *shape[1:]] for s in range(nranks - 1)]
        shapes += [[shape[0] % k, *shape[1:]]]
    else:
        shapes = [[k, *shape[1:]] for s in range(nranks)]
    if rank == 0:
        print("Partitioning into the following shapes: ", shapes)
    local_shape = shapes[rank]
    boundaries = np.split(boundary, [k for f in shapes[1:]], axis=1)
    sol = Solution(lattice, nu, local_shape, boundaries[rank], dx, dt)
    return sol
