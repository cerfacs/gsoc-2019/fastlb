"""
stream.py

Streaming step of the LBM solver
"""
import tensorflow as tf

from .utils import simple_conv, pad_mobius


def get_stream_step(sol, lattice, Cs):
    """Assemble the streaming step of the LBM solver"""
    # stream f
    f_pad = sol.f_pad
    f_pad = simple_conv(f_pad, lattice.stream)
    # calc new velocity and density
    rho = tf.expand_dims(tf.reduce_sum(f_pad, lattice.dim+1), lattice.dim+1)
    vel = simple_conv(f_pad, lattice.lveloc)
    vel /= Cs * rho

    # create steps
    stream_step = sol.f.assign(f_pad)
    rho_step = sol.rho.assign(rho)
    vel_step = sol.velocity.assign(vel)
    step = tf.group(*(stream_step, rho_step, vel_step))
    return step
