"""
solve.py

Solve an LBM setup.
This is a high level function which assembles many sublayers.
"""
import tensorflow as tf
import tqdm

from .stream import get_stream_step
from .collide import get_collide_step


def solve(sol, niter, user_init_step, setup_step, save_step, save_interval):
    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    sess.run(sol.initialize_step())
    sess.run(user_init_step)

    stream_step = get_stream_step(sol, sol.lattice, sol.Cs)
    collide_step = get_collide_step(sol, sol.boundary, sol.lattice, sol.Cs)

    # run solver
    sess.run(stream_step)
    num_steps = int(niter / sol.dt)
    time = 0.
    for i in tqdm.tqdm(range(num_steps)):
        if int(time / save_interval) > int((time - sol.dt) / save_interval):
            save_step(sol, sess, i)
        sess.run(setup_step)
        sess.run(collide_step)
        sess.run(stream_step)
        time += sol.dt


def mpisolve(sol, niter, user_init_step, setup_step, save_step, save_interval):
    from mpi4py import MPI
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())

    sess.run(sol.initialize_step())
    sess.run(user_init_step)

    stream_step = get_stream_step(sol, sol.lattice, sol.Cs)
    collide_step = get_collide_step(sol, sol.boundary, sol.lattice, sol.Cs)

    # run solver
    sess.run(stream_step)
    num_steps = int(niter / sol.dt)
    time = 0.
    for i in tqdm.tqdm(range(num_steps)):
        if int(time / save_interval) > int((time - sol.dt) / save_interval):
            save_step(sol, sess)
        sess.run(setup_step)
        sess.run(collide_step)
        sess.run(stream_step)
        time += sol.dt
    if rank == 0:
        data = numpy.arange(100, dtype=numpy.float64)
        comm.Send(data, dest=1, tag=13)
    elif rank == 1:
        data = numpy.empty(100, dtype=numpy.float64)
        comm.Recv(data, source=0, tag=13)
