# FastLB

A fast Lattice Boltzmann fluid flow solver written in python, heavily
leveraging the Tensorflow library.

To install (with nitrox access):

`pip install git+ssh://git@gitlab.com/cerfacs/fastlb.git`

Inspired by the
[work](https://github.com/loliverhennigh/Lattice-Boltzmann-fluid-flow-in-Tensorflow)
by Oliver Hennigh.
