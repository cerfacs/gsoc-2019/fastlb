#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='fastlb',
    version='0.1.0',
    description='Lattice Boltzman solver using Tensorflow',
    long_description=readme,
    author='CERFACS',
    author_email='coop@cerfacs.fr',
    url='https://nitrox.cerfacs.fr/open-source/fastlb',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
    install_requires=[
        'numpy',
        'opencv-python',
        'tensorflow',
        'tqdm',
        ],
)

