# -*- coding: utf-8 -*-

import sys
import os
from pathlib import Path

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import fastlb
from fastlb import utils
from fastlb.solution import Solution, mpipartition
from fastlb.solve import solve, mpisolve

DATA_PATH = Path(os.path.dirname(__file__)).resolve() / 'data'
