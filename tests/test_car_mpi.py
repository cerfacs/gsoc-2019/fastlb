"""test_car_steady.py

Functional test with a steady state car setup
"""
import time

import numpy as np
import tensorflow as tf
import cv2

from .context import Solution, mpipartition
from .context import solve, mpisolve
from .context import utils
from .context import DATA_PATH


def make_car_boundary(shape, car_shape):
    img = cv2.imread(str(DATA_PATH / "car.png"), 0)
    img = cv2.flip(img, 1)
    resized_img = cv2.resize(img, car_shape)
    resized_img = -np.rint(resized_img/255.0).astype(int).astype(np.float32) + 1.0
    resized_img = resized_img.reshape([1, car_shape[1], car_shape[0], 1])
    boundary = np.zeros((1, shape[0], shape[1], 1), dtype=np.float32)
    boundary[:, shape[0]-car_shape[1]:, 32:32+car_shape[0], :] = resized_img
    #boundary[:,0,:,:] = 1.0
    boundary[:,shape[0]-1,:,:] = 1.0
    return boundary

def car_init_step(sol, value=0.08):
    vel_dir = tf.zeros_like(sol.velocity[:,:,:,0:1])
    vel = tf.concat([vel_dir+value, vel_dir, vel_dir], axis=3)
    vel_dot_vel = tf.expand_dims(tf.reduce_sum(vel * vel, axis=3), axis=3)
    vel_dot_c = (tf.reduce_sum(tf.expand_dims(vel, axis=3) *
                 tf.reshape(sol.lattice.lveloc,
                            [1,1,1,sol.lattice.nneighbors,3]), axis=4))
    feq = (tf.reshape(sol.lattice.weights,
                      [1,1,1,sol.lattice.nneighbors]) *
           (1.0 + 3.0*vel_dot_c/sol.Cs +
            4.5*vel_dot_c*vel_dot_c/(sol.Cs*sol.Cs) -
            1.5*vel_dot_vel/(sol.Cs*sol.Cs)))

    vel = vel * (1.0 - sol.boundary)
    rho = (1.0 - sol.boundary)

    f_step = sol.f.assign(feq)
    rho_step = sol.rho.assign(rho)
    vel_step = sol.velocity.assign(vel)
    initialize_step = tf.group(*[f_step, rho_step, vel_step])
    return initialize_step

def car_setup_step(sol, value=0.001):
    shape = sol.shape
    u = np.zeros((1,shape[0],1,1))
    l = shape[0] - 2
    for i in range(shape[0]):
        yp = i - 1.5
        vx = value*4.0/(l*l)*(l*yp - yp*yp)
        #vx = value
        u[0,i,0,0] = vx
    u = u.astype(np.float32)
    u = tf.constant(u)

    # input vel on left side
    f_out = sol.f[:,:,1:]
    f_edge = tf.split(sol.f[:,:,0:1], 9, axis=3)

    # new in distrobution
    rho = (f_edge[0] + f_edge[2] + f_edge[4] + 2.0*(f_edge[3] + f_edge[6] + f_edge[7]))/(1.0 - u)
    f_edge[1] = f_edge[3] + (2.0/3.0)*rho*u
    f_edge[5] = f_edge[7] + (1.0/6.0)*rho*u - 0.5*(f_edge[2]-f_edge[4])
    f_edge[8] = f_edge[6] + (1.0/6.0)*rho*u + 0.5*(f_edge[2]-f_edge[4])
    f_edge = tf.stack(f_edge, axis=3)[:, :, :, :, 0]
    f = tf.concat([f_edge, f_out], axis=2)

    # new Rho
    rho = sol.rho
    rho_out = rho[:,:,1:]
    rho_edge = tf.expand_dims(tf.reduce_sum(f_edge, axis=3), axis=3)
    rho = tf.concat([rho_edge,rho_out], axis=2)

    # new vel
    vel = sol.velocity
    vel_out = vel[:,:,1:]
    vel_edge = utils.simple_conv(
            f_edge, tf.reshape(sol.lattice.lveloc,
            [1,1,sol.lattice.nneighbors, 3]))
    vel_edge = vel_edge/rho_edge
    vel = tf.concat([vel_edge,vel_out],axis=2)

    # make steps
    f_step = sol.f.assign(f)
    rho_step = sol.rho.assign(rho)
    vel_step = sol.velocity.assign(vel)
    setup_step = tf.group(*[f_step, rho_step, vel_step])
    return setup_step

def save_func(dest):
    def car_save(sol, sess, it):
        frame = sess.run(sol.velocity)
        frame = np.sqrt(np.square(frame[0,:,:,0]) + np.square(frame[0,:,:,1]) + np.square(frame[0,:,:,2]))
        np.save(dest, frame)
    return car_save

def run(shape, dest):
    # constants
    input_vel = 0.1
    nu = input_vel * 0.5
    boundary = make_car_boundary(shape=shape, car_shape=(int(shape[1]/1.6), int(shape[0]/2.3)))

    # sol
    sol = mpipartition("D2Q9", nu, shape, boundary, dx=1.0, dt=1.0)

    return
    # make lattice state, boundary and input velocity
    initialize_step = car_init_step(sol, value=0.08)
    setup_step = car_setup_step(sol, value=input_vel)
    car_save = save_func(dest)

    # run steps
    mpisolve(sol, 61, initialize_step, setup_step, car_save, 60)


if __name__ == '__main__':
    shape = [32, 128]
    run(shape, 'car_steady_mpi')
