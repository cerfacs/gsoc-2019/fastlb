"""test_car_steady.py

Functional test with a steady state car setup
"""
import time

import numpy as np
import tensorflow as tf
import cv2

from .context import Solution
from .context import solve
from .context import utils
from .context import DATA_PATH


def make_cube_boundary(shape):
    boundary = np.zeros([1] + shape + [1], dtype=np.float32)
    boundary[:,
             shape[0] // 2 - 20:shape[0] // 2 + 20,
             shape[1] // 2 - 20:shape[1] // 2 + 20,
             shape[0] // 2 - 5:shape[0] // 2 + 4] = 1.0
    return boundary


def cube_init_step(sol, value=0.04):
    vel_dir = tf.zeros_like(sol.velocity[:,:,:,:,0:1])
    vel = tf.concat([vel_dir+value, vel_dir, vel_dir], axis=4)
    vel_dot_vel = tf.expand_dims(tf.reduce_sum(vel * vel, axis=4), axis=4)
    vel_dot_c = tf.reduce_sum(tf.expand_dims(vel, axis=4) * sol.lattice.lveloc, axis=5)
    feq = sol.lattice.weights * (1.0 + (3.0/sol.Cs)*vel_dot_c + (4.5/(sol.Cs*sol.Cs))*(vel_dot_c*vel_dot_c) - (1.5/(sol.Cs*sol.Cs))*vel_dot_vel)

    vel = vel * (1.0 - sol.boundary)
    rho = (1.0 - sol.boundary)

    f_step = sol.f.assign(feq)
    rho_step = sol.rho.assign(rho)
    vel_step = sol.velocity.assign(vel)
    initialize_step = tf.group(*[f_step, rho_step, vel_step])
    return initialize_step


def cube_setup_step(sol, value=0.004):
    shape = sol.shape
    u = np.zeros((1,shape[0],shape[1],1,1))
    for i in range(shape[0]):
        for j in range(shape[1]):
            u[0,i,j,0] = value
    u = u.astype(np.float32)
    u = tf.constant(u)

    # new vel
    vel = sol.velocity
    vel_out = vel[:,:,:,1:]
    vel_edge = vel[:,:,:,:1]
    vel_edge = tf.split(vel_edge, 3, axis=4)
    vel_edge[0] = vel_edge[0]+value
    vel_edge = tf.concat(vel_edge, axis=4)
    vel = tf.concat([vel_edge,vel_out],axis=3)

    # make steps
    vel_step = sol.velocity.assign(vel)
    return vel_step


def save_func(shape, dest):
    def cube_save(sol, sess, it):
        frame = sess.run(sol.velocity)
        frame = np.sqrt(
            np.square(frame[0, :, shape[1] // 2, :, 0]) +
            np.square(frame[0, :, shape[1] // 2, :, 1]) +
            np.square(frame[0, :, shape[1] // 2, :, 2]))
        np.save(dest, frame)
    return cube_save


def run(shape, dest):
    input_vel = 0.01
    nu = input_vel * 0.0015
    boundary = make_cube_boundary(shape=shape)
    sol = Solution("D3Q19", nu, shape, boundary)

    # make lattice state, boundary and input velocity
    initialize_step = cube_init_step(sol, value=input_vel)
    setup_step = cube_setup_step(sol, value=input_vel)
    cube_save = save_func(shape, dest)

    # start solver
    solve(sol, 11, initialize_step, setup_step, cube_save, 10)


def test_cube_runs(tmp_path):
    shape = [64, 64, 16]
    run(shape, tmp_path / 'cube')


def test_cube_result_identical(tmp_path):
    shape = [64, 64, 16]
    run(shape, tmp_path / 'cube')
    output = np.load(tmp_path / 'cube.npy')
    target = np.load(DATA_PATH / 'cube.npy')
    assert np.allclose(output, target)
